% 本程序采用manning方法计算FCC结构的“空位风G”和“示踪原子扩散系数比”
% Phys. Rev. B 85, 174111 (2012) , Auther: T.P.C. Klaver
% 溶质浓度
Cs = 0.01;
a0 = 3.545e-10;
Vat = a0^3 / 4;
kb = 8.625E-5;
Temp = [0:50:1800];
KT = Temp.* kb;
KTV = KT.*Vat;
Cv = exp(-1.8./KT);
f0 = 0.7815; 
a = [0, 1338.0577, 924.3303, 180.3121, 10];
b = [435.2839, 595.9725, 253.3, 40.1478, 2];
FF = 7 - (a(2)+a(3)+a(4)+a(5)) / (b(1)+b(2)+b(3)+b(4)+b(5));
%% Cr - 输入特征能量
%Em0(1:3) = [0.73, 1.15, 1.41];
%Em1(1:3) = [0, 0.67, 0.72];
%Em2(1:3) = [0.56, 0.83, 0.74];
%Ebv(1:3) = [0.01, -0.08, -0.07];

Em0(1:3) = [0.73, 1.15, 1.41];
Em1(1:3) = [0, 0.6729, 0.7318];
Em2(1:3) = [0.5598, 0.8288, 0.7417];
Ebv(1:3) = [0.0038, -0.0906, -0.0747];


Hb1Ts(1:3) = [0.743, 0.743, 0.743] - Em1 + Ebv;
Hb2Ts(1:3) = Em0 - Em2 + Ebv;

Hb1mean = (Hb1Ts(2) + Hb1Ts(3)) / 2;
Hb2mean = (Hb2Ts(1) + Hb2Ts(2)) / 2;

param1 = exp(Hb1mean./KT);
param2 = exp(Hb2mean./KT);
fB = (2.*param1 + FF)./ (2.*param1 + 2.*param2 + FF);

Gwind = ((6 - 4.*param1)./ ( 2.*param1 + FF))';
Ratio = (fB.*param2/f0)';
% 反向求解LAA， LVV， LBB， LVB， LAB
E0_mean = (Em0(1) + Em0(2)) / 2; w0_mean = exp(-E0_mean./KT); % w3_mean = w0_mean;
Eb1_mean = (Ebv(2) + Ebv(3)) / 2; wb1_mean = exp(-Eb1_mean./KT);
Eb2_mean = (Ebv(1) + Ebv(2)) / 2; wb2_mean = exp(-Eb2_mean./KT);
E2_mean = (Em2(1) + Em2(2)) / 2; w2_mean = exp(-E2_mean./KT);
E1_mean = (Em1(2) + Em1(3)) / 2; w1_mean = exp(-E1_mean./KT);

DB = (a0^2.*w2_mean./wb2_mean.*fB.*Cv)';
DA0 = (a0^2.*w0_mean.*f0.*Cv)';

W0_Cr = w0_mean;
W1_Cr = w1_mean;
W2_Cr = w2_mean;
W3_Cr = w0_mean;

XX_Cr = -19 + (W2_Cr.*(4*W1_Cr + 14*W3_Cr) + 40*W3_Cr.*(W1_Cr+W3_Cr))./(W3_Cr.*(W1_Cr + W2_Cr + 3.5*W3_Cr));
DA = DA0.*(1+Cs.*XX_Cr)';

Ratio2 = (DB./DA);

LBB = DB .* Cs ./ KTV';
LAA = DA ./ KTV';
LAB = Gwind .* LBB;
LAV = -(LAA + LAB);
LBV = -(LBB + LAB);
                        
LVV = -(LAV + LBV); 
LVV0 = DA0 ./ KTV';

DV_Ratio = double(LVV) ./ double(LVV0);

%作图
figure(1)
plot(Temp', (Ratio)) %
hold on
plot(Temp', (Ratio2)) %
legend('Cr—Ratio','Cr-Ratio2')
%set(gca, 'yscale', 'log')
axis([0 1800 0 5])
figure(2)
plot(Temp', Gwind)
hold on
legend('Cr-mean')
axis([0 1800 -2 2])

maningCr = [Temp', Gwind, log10(Ratio), log10(Ratio2), DV_Ratio];
